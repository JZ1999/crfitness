# Generated by Django 2.1.7 on 2019-06-08 00:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ejercicio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(default=' ', max_length=50)),
                ('descripcion', models.TextField(default=' ')),
                ('repeticiones', models.IntegerField(default=1)),
                ('duracion', models.FloatField(default=1)),
                ('descanso', models.FloatField(default=1)),
            ],
        ),
    ]
