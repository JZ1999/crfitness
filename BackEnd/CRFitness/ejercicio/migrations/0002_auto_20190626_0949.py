# Generated by Django 2.1.7 on 2019-06-26 15:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ejercicio', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ejercicio',
            old_name='duracion',
            new_name='series',
        ),
    ]
