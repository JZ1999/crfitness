from .models import Ejercicio
from django.contrib.auth import authenticate
from rest_framework import exceptions, serializers
from rest_framework.response import Response
class  EjercicioSerializer(serializers.ModelSerializer):
	class Meta:
		model  = Ejercicio
		fields  = '__all__'
