from .models import Ejercicio
from .serializer import EjercicioSerializer
from django.shortcuts import render
from rest_framework import viewsets

class EjercicioViewSet(viewsets.ModelViewSet):
	queryset = Ejercicio.objects.all()
	serializer_class = EjercicioSerializer

