from .views import EjercicioViewSet 
from django.urls import path
from rest_framework import routers

router  = routers.SimpleRouter()

router.register('ejercicio',EjercicioViewSet)

urlpatterns =  router.urls + [
]
