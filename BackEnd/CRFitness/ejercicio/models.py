from django.db import models

class Ejercicio(models.Model):
	nombre = models.CharField(max_length=50, blank=False, default=" ")
	descripcion = models.TextField(default= " ")
	repeticiones = models.IntegerField(default = 1)
	series = models.FloatField(default = 1)
	descanso = models.FloatField(default = 1)

	def __str__(self):
		return 'Nombre: {}. \n Repeticiones: {} \n Series: {} \n Descanso: {} minutos.'.format(self.nombre, self.repeticiones, self.series, self.descanso)
