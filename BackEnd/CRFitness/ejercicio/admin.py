from .models import Ejercicio
from django import forms
from django.contrib import admin

class EjercicioAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    ordering = ['nombre']

admin.site.register(Ejercicio, EjercicioAdmin)


