from django import forms
from django.db import models

class DatosCliente(models.Model):
	peso = models.FloatField(default = 0)
	altura = models.FloatField(default = 0)
	IMC =  models.FloatField(default = 0)
	estadoIMC  = models.TextField(default = ' ')

	def clean(self):
		self.IMC = round(self.peso/(self.altura*0.01)**2, 2)
		self.estadoIMC = DatosCliente.setEstadoIMC(self.IMC)

	def setPesoAltura(self, nuevoPeso, nuevoAltura, id):
		objDatoCliente = DatosCliente.objects.get(pk = id)
		pesoViejo = getattr(objDatoCliente, "peso")
		alturaViejo = getattr(objDatoCliente, "altura")
		IMC = getattr(objDatoCliente, "IMC")
		estadoIMC = getattr(objDatoCliente, "estadoIMC")

		if not nuevoPeso:
			nuevoPeso = pesoViejo
		if not nuevoAltura:
			nuevoAltura = alturaViejo
		nuevoIMC = (nuevoPeso) / (nuevoAltura*0.01)**2
		estadoNuevo = DatosCliente.setEstadoIMC(nuevoIMC)

		objDatoCliente.peso = nuevoPeso
		objDatoCliente.altura = nuevoAltura
		objDatoCliente.IMC = nuevoIMC
		objDatoCliente.estadoIMC = estadoNuevo
		objDatoCliente.save()

	def setEstadoIMC(IMCNuevo):
		if IMCNuevo <= 15:
			return "Delgadez muy severa"
		elif IMCNuevo < 16:
			return "Delgadez severa"
		elif IMCNuevo < 18.5:
			return "Delgadez"
		elif IMCNuevo < 25:
			return "Peso saludable"
		elif IMCNuevo < 30:
			return "Sobrepeso"
		elif IMCNuevo < 35:
			return "Obesidad severa"
		return "Obesidad mórbida"

	def __str__(self):
		return 'Peso: {}.  Altura: {}.  IMC: {}.\nInformación sobre su estado: {}.'.format(self.peso,
			self.altura, self.IMC, self.estadoIMC)

class FormDatosCliente(forms.ModelForm):
	class Meta:
		model = DatosCliente
		exclude = ('IMC','estadoIMC',)