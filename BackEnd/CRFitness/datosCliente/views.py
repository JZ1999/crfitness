from .models import DatosCliente
from .serializer import DatosClienteSerializer
from cliente import models as modelsCL
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
import json
class DatosClienteViewSet(viewsets.ModelViewSet):
	queryset = DatosCliente.objects.all() 
	serializer_class = DatosClienteSerializer

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def actualizarPesoAltura(request):
	"""
	recibe:
	id del cliente a modificar
	peso a modificar o
	altura a modificar
	"""
	idCliente = int(request.POST.get("id", ""))
	cliente = get_object_or_404(modelsCL.Cliente, pk = idCliente)
	try:
		nAltura = int(request.POST.get("altura", ""))
	except:
		nAltura = 0
	try:
		nPeso = int(request.POST.get("peso", ""))
	except:
		nPeso = 0
	idDatosCliente = cliente.datos.id
	datosObjeto = DatosCliente.objects.get(pk = idDatosCliente)
	datosObjeto.setPesoAltura(nuevoPeso = nPeso, nuevoAltura = nAltura, id = idCliente)
	return HttpResponse(status = 200)
