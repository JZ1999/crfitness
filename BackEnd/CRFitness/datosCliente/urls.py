from .views import DatosClienteViewSet, actualizarPesoAltura
from django.urls import path
from rest_framework import routers

router  = routers.SimpleRouter()

router.register('datosCliente',DatosClienteViewSet)

urlpatterns =  router.urls  + [ 
path('actualizarPesoAltura', actualizarPesoAltura, name='actualizarPesoAltura')
]

