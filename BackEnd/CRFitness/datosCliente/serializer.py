from .models import DatosCliente
from rest_framework import serializers

class  DatosClienteSerializer(serializers.ModelSerializer):
	class Meta:
		model = DatosCliente
		fields = '__all__'
		read_only_fields = ('IMC','estadoIMC')

	def create(self, validated_data, **kwargs):
		validated_data['IMC'] = validated_data['peso']/(validated_data['altura']*0.01)**2
		validated_data['estadoIMC'] = DatosCliente.setEstadoIMC(validated_data['IMC'])
		return DatosCliente.objects.create(**validated_data)
