from .models import DatosCliente, FormDatosCliente
from django.contrib import admin


class DatosClienteAdmin(admin.ModelAdmin):
	form = FormDatosCliente
	
admin.site.register(DatosCliente, DatosClienteAdmin)
