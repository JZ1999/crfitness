from .views import NovedadViewSet
from rest_framework import routers

router  = routers.SimpleRouter()

router.register('novedad',NovedadViewSet)

urlpatterns =  router.urls
