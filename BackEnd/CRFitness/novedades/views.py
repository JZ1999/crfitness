from .models import Novedad
from .serializer import NovedadSerializer
from rest_framework import viewsets

class NovedadViewSet(viewsets.ModelViewSet):
	queryset = Novedad.objects.all()
	serializer_class = NovedadSerializer
