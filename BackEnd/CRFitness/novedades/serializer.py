from .models import Novedad
from rest_framework import serializers

class  NovedadSerializer(serializers.ModelSerializer):
	class Meta:
		model = Novedad
		fields = '__all__'

		
