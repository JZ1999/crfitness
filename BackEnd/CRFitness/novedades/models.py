from django.db import models

class Novedad(models.Model):
	titulo = models.CharField(max_length=20)
	descripcion = models.TextField()
	ilustracion = models.ImageField(upload_to = 'novedades' , blank = True)

	class Meta:
		verbose_name_plural = "novedades"

	def __str__(self):
		return 'Título: {}.'.format(self.titulo)
