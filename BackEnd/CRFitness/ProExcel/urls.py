from .views import upload_excel
from django.urls import path
from rest_framework import routers

router  = routers.SimpleRouter()


urlpatterns =  router.urls + [path('ProExcel', upload_excel , name='excel')]
