from .models import UploadFileForm, getValues, save_values_in_db
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
import xlrd

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def upload_excel(request):
    form = UploadFileForm(request.POST, request.FILES)
    if form.is_valid():
        input_excel = request.FILES['file']
        book = xlrd.open_workbook(file_contents=input_excel.read())
        values = getValues(book)
        save_values_in_db(values)
        return render_to_response('admin/success.html')
    print(form.errors.as_data())
    return render_to_response('admin/fail.html', {'errors':form.errors['file']})
