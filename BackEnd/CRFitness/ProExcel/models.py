from cliente import models as modelCL
from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from Usuario import models as modelUS
import Usuario
import xlrd

def validate_format(value):
    if not value.name.endswith('.xlsx'):
        raise ValidationError('El archivo no es .xlsx')

class UploadFileForm(forms.Form):
    file = forms.FileField(validators=[validate_format])

def getValues(workBook):
    '''
    Parameters: workBook -  xlrd.book.Book
    returns a matrix with all the information
    '''

    for sheet in workBook.sheets():
        values = []
        for row in range(sheet.nrows):
            col_value = []
            for col in range(sheet.ncols):
                value  = (sheet.cell(row,col).value)
                try : value = str(int(value))
                except : pass
                col_value.append(value)
            values.append(col_value)
    return values

def save_values_in_db(values):
    '''
    Parameters: values - a matrix with data in ExcelDB Format

    Saves the data in the ExcelDB database
    '''

    #               data without the headers
    for value in values[1:]:
        try:
            user = modelUS.User.objects.get(email = value[5])
        except:
            nuevoUser = get_user_model().objects.create_user(email=value[5], password=passwordgenerator(value))
            nuevoCliente = modelCL.Cliente(nombre = value[3], user = nuevoUser )
            nuevoCliente.save()

def passwordgenerator(value):
    '''
    Recibe los datos de un cliente
    retorna un password, string, con las primeras 4 letras del correo
    y los primeros 4 digitos de la cedula
    '''
    return str(value[2])[:8]
