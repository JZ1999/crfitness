from django.apps import AppConfig


class ProexcelConfig(AppConfig):
    name = 'ProExcel'
