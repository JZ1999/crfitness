from .models import Comentarios
from .serializer import ComentariosSerializer
from rest_framework import viewsets

class ComentariosViewSet(viewsets.ModelViewSet):
	queryset = Comentarios.objects.all()
	serializer_class = ComentariosSerializer
