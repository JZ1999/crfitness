from django import forms
from django.db import models
from django.forms import ModelForm
from Usuario import models as modelsUS

class Comentarios(models.Model):
	nombre = models.CharField(max_length=40)
	correo = models.CharField(max_length=40) # || foreigkey apuntando a un correo
	mensaje = models.TextField()
	paraNombre = models.CharField(max_length=40, blank=True)
	paraEmail = models.EmailField(max_length=40, blank=True)
	def __str__(self):
		return 'Nombre: {}.  Email: {}.'.format(self.nombre, self.correo[:20])
	
	class Meta:
		verbose_name_plural = "clientes"

		
class ComentarioForm(forms.ModelForm):
	class Meta:
		model = Comentarios
		fields  = '__all__'