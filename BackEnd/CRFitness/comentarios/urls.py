from .views import ComentariosViewSet
from rest_framework import routers

router  = routers.SimpleRouter()

router.register('comentarios',ComentariosViewSet)

urlpatterns =  router.urls
