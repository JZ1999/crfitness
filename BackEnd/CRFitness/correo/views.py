from .models import Correo
from .serializer import CorreoSerializer
from cliente import models as modelsCL
from comentarios.models import ComentarioForm
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage, send_mail
from django.http import JsonResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string, get_template
from django.views.generic import TemplateView
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from Usuario import models as modelsUS


class CorreoViewSet(viewsets.ModelViewSet):
    queryset = Correo.objects.all()
    serializer_class = CorreoSerializer


@api_view(['POST'])
def enviarCorreo(request):
    correo = request.data.get("correo", None)
    nombre = request.data.get("nombre", None)
    mensaje = request.data.get("mensaje", None)
    paraNombre = request.data.get("adminName", "CRfitness")
    paraCorreo = request.data.get("adminEmail", "")
    if request.method == "POST":
        newCorreo = ComentarioForm(request.POST)
        if newCorreo.is_valid():
            newCorreo.save()
        res = send_mail('Mensaje de contáctanos CRFitness de %s' % nombre,
                        'De %s\nPara: %s %s\nCorreo %s\n\n%s' % (nombre, paraCorreo, paraNombre, correo, mensaje),
                        Correo.correoDest, ['crfitnesssrl@gmail.com', paraCorreo],
                        fail_silently=False)
        res = {"response": res}
        return JsonResponse(res)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def enviarCorreo_login(request):
    """
    recibe un request con:
    (id) del admin
    (nombre) del quien envia el comentario
    (comentario)

     y

     envia el correo al administrado con el comentario

     retorna un 200 si lo envia de lo contrario un error

    """

    idDelReceptor = request.data.get("idDelReceptor", None)  ###1
    idDelEmisor = request.data.get("idDelEmisor", None)  ####2 ###3
    clienteDelEmisor = modelsCL.Cliente.objects.get(pk=int(idDelEmisor))  ###2 ##3
    usuarioDelReceptor = modelsUS.User.objects.get(pk=int(idDelReceptor))  ##1
    usuarioDelEmisor = modelsUS.User.objects.get(pk=clienteDelEmisor.user)  ##2
    correoDelReceptor = usuarioDelReceptor.email  # 1
    correoDelEmisor = usuarioDelEmisor.email  # 2
    nombreDelEmisor = clienteDelEmisor.nombre  # 3
    comentarioDelEmisor = request.data.get("mensaje", None)  # 4
    mensajeDelEmisor = send_mail('Mensaje de un Usuario de CRFitness',
                                 ('De: %s\n\n%s') % (nombreDelEmisor, comentarioDelEmisor),
                                 correoDelEmisor, [correoDelReceptor, "crfitnesssrl@gmail.com"],
                                 fail_silently=False)
    comprobacion = {"response": mensajeDelEmisor}
    return JsonResponse(comprobacion)
