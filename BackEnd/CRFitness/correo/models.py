from django.db import models

class Correo(models.Model):
	correoDest = "joseph.zamora64@yahoo.com"
	nombre = models.TextField(default = ' ')
	correo = models.TextField(default = ' ')
	mensaje =  models.TextField(default = ' ')

	def __str__(self):
		return 'Nombre: {}.  Email: {}.'.format(self.nombre, self.correo)
