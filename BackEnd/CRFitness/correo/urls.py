from .views import CorreoViewSet, enviarCorreo
from django.urls import path
from rest_framework import routers

router = routers.SimpleRouter()

router.register('correo', CorreoViewSet)

urlpatterns = router.urls + [
		path('enviarCorreo', enviarCorreo, name='enviarCorreo'),
	]