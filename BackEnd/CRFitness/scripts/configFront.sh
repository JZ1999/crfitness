#!/usr/bin/env sh
# Un script para compilar el código frontend angular
# y enviarlo a las carpetas respectivas en el servidor
# de django en CRFitness/angular/static/angular
# y manda las imágenes a CRFitness/assets.
# tambíen corre un script en python3 llamado
# agregarJinja.py que hace modificaciones al
# inicio.html para agregarle el código jinja
# que ocupa para compilar bien en el servidor de
# django

echo "Cambiando a ~/Proyects/CRFitness/FrontEnd/CRFitness/"
cd ~/Proyects/CRFitness/FrontEnd/CRFitness/

echo "Eliminando archivos estaticos anteriores"
rm ~/Proyects/CRFitness/BackEnd/CRFitness/angular/static/angular/*
rm ~/Proyects/CRFitness/BackEnd/CRFitness/angular/templates/angular/index.html

ng build --prod --output-path ~/Proyects/CRFitness/BackEnd/CRFitness/angular/static/angular && cp ~/Proyects/CRFitness/BackEnd/CRFitness/angular/static/angular/assets/* ~/Proyects/CRFitness/BackEnd/CRFitness/assets/
mv ~/Proyects/CRFitness/BackEnd/CRFitness/angular/static/angular/index.html ~/Proyects/CRFitness/BackEnd/CRFitness/angular/templates/angular/
echo "Build listo"

echo "Corriendo script agregarJinja"
python3 ~/Proyects/CRFitness/BackEnd/CRFitness/scripts/agregarJinja.py ~/Proyects/CRFitness/BackEnd/CRFitness/angular/templates/angular/index.html

python3 ~/Proyects/CRFitness/BackEnd/CRFitness/manage.py collectstatic

cd ~/Proyects/CRFitness/BackEnd/CRFitness

echo "listo..."
