from cliente import models as modelsCL
from rutina import models as modelsRT
from Usuario import models as modelsUS
from datosCliente import models as modelsDT
from ejercicio import models as modelsEJ
from foroComentario import models as modelsFC
from random import randint, shuffle
import names

def vaciarBase(cliente = True, rutina = True, ejercicio = True, datosCliente = True, foroComentario = True, user = True):
	"""
	recibe como parametros
	6 booleanos de lo que quiere borrar
	(no inserte nada) para dejar los
	valor por default como True y borrar 
	la mayor parte de la base de datos
	(admin no seran borrados)
	de lo contrario inserte False si 
	quiere conservar alguna tabla intacta

	cliente
	rutina
	ejercicio
	datosCliente
	foroComentario
	user (user no borrara nunca los administradores)

	no retorna nada 
	"""
	if cliente:
		vaciarCliente()
	if rutina:
		vaciarRutina()
	if ejercicio:
		vaciarEjercicio()
	if datosCliente:
		vaciarDatosCliente()
	if foroComentario:
		vaciarForoComentario()
	if user:
		vaciarUser()

def vaciarCliente():
	"""
	no recibe nada
	elimina todo dato de 
	la tabla Cliente
	"""
	while modelsCL.Cliente.objects.first():
		modelsCL.Cliente.objects.first().delete()

def vaciarRutina():
	"""
	no recibe nada
	elimina todo dato de 
	la tabla Rutina
	"""
	while modelsRT.Rutina.objects.first():
		modelsRT.Rutina.objects.first().delete()

def vaciarDatosCliente():
	"""
	no recibe nada
	elimina todo dato de 
	la tabla datosCliente
	"""
	while modelsDT.DatosCliente.objects.first():
		modelsDT.DatosCliente.objects.first().delete()

def vaciarEjercicio():
	"""
	no recibe nada
	elimina todo dato de 
	la tabla Ejercicios
	"""
	while modelsEJ.Ejercicio.objects.first():
		modelsEJ.Ejercicio.objects.first().delete()

def vaciarForoComentario():
	"""
	no recibe nada
	elimina todo dato de 
	la tabla ForoComentario
	"""
	while modelsFC.Comentario.objects.first():
		modelsFC.Comentario.objects.first().delete()

def vaciarUser(borrar = False):
	"""
	recibe valor bolleano "borrar":
	es False por defecto
	si desea borrar los administradores
	inserte True
	elimina todo dato requerido de 
	la tabla usuario
	"""
	listaUsuarios = modelsUS.User.objects.all()

	for usuario in listaUsuarios:
		if borrar or not usuario.is_staff:
			usuario.delete()

def llenarBase():
	"""
	llena con 20 objetosl
	la mayoria de tablas
	de la base de datos
	clientes
	rutinas
	datoscliente
	forocomentarios
	ejercicios
	usuarios

	no retorna nada
	"""
	llenarForoComentario(20)

def llenarEjercicio(n):
	"""
	crea n cantidad de ejercicios
	los datos son al azar entre la
	informacion que le agregamos
	no retorna nada
	"""
	repeticiones = list(range(5,30))
	duracion = list(range(5,30))
	descanso  = list(range(5,30))
	listaNombreR = ["Sentadillas","lagartijas","Fliexiones", "Abdominales", "Correr", "Saltos doble comba" ]
	while n:
		shuffle(listaNombreR)
		shuffle(repeticiones)
		shuffle(duracion)
		shuffle(descanso)
		nuevoEjercicio = modelsEJ.Ejercicio(descanso =  descanso[0], nombre = listaNombreR[0], repeticiones = repeticiones[0], duracion = duracion[0], descripcion = "Una sentadilla es un ejercicio físico que se lleva a cabo para desarrollar los músculos y fortalecer los tendones y los ligamentos de las piernas. También permite tonificar los glúteos y aporta beneficios a la cadera. ... La media sentadilla, en cambio, consiste en realizar una flexión menos acentuada.")
		nuevoEjercicio.save()
		n-=1

def llenarForoComentario(n):
	"""
	cliente  =  models.ForeignKey(cliente_models.Cliente, on_delete=models.CASCADE)
    ESTRELLAS_CHOISES = [(1,1),(2,2),(3,3),(4,4),(5,5)]
    estrellas = models.IntegerField(choices=ESTRELLAS_CHOISES)
    comentario  = models.TextField(default = ' ')
    fecha = models.DateField(auto_now = True)


    crea n cantidad de ForoComentarios
    los datos son al azar entre la
    informacion dada al sistema
    no retorna nada
    por dependencia a cliente esta 
    funcion genera:
     n clientes
     n usuarios
     20 ejercicios
     20 rutinas
     20 datosCliente

	"""
	vaciarBase()
	llenarCliente(n)
	listaC = modelsCL.Cliente.objects.all()
	listaCLiente = []
	for Cliente in listaC:
		listaCLiente.append(Cliente)
	estrellas = [1,2,3,4,5]
	comentario = "En la novela de Philip K. Dick Podemos recordarlo todo por usted (que luego sería adaptada al cine en Desafío total) un hombre cualquiera se somete a un implante de memoria para tener recuerdos emocionantes de una vida que no tuvo. Este año hemos asistido a un hackeo similar, en el que gracias a un programa desarrollado por las Universidades de Oxford, Genova y Berkeley, se ha accedido a información almacenada en el cerebro (número secreto de la tarjeta de crédito, dirección postal o la fecha de nacimiento). El experimento tuvo un éxito de entre el 10 y el 40 por ciento encontrando esta información."
	e = 2
	while n:
		shuffle(listaCLiente)
		shuffle(estrellas)
		nuevoForoComentario = modelsFC.Comentario(cliente = listaCLiente[0], estrellas = estrellas[0], comentario = comentario).save()
		nuevoForoComentario.save()
		n-=1


def llenarRutina(n):
	"""
	crea n cantidad de rutinas
	se le asignara un 7 a n
	si el n ingresado es menor que 7
	para que tenga el minimo de 
	rutinas a la semana
	escogiendo al azar cantidades
	aleatorias de ejercicios y 
	un dia al azar
	exceptuando los primeros 7 dias

	no retorna nada
	"""
	n|=7
	llenarEjercicio(20)
	dias = ["L","K","M","J","V","S","D"]
	lEjercicios = modelsEJ.Ejercicio.objects.all()
	listaEjercicios = []

	for ejercicio in lEjercicios:
		listaEjercicios.append(ejercicio)

	for diaSeguros in dias:
		nuevaRutina = modelsRT.Rutina(dia = diaSeguros)
		nuevaRutina.save()
		shuffle(listaEjercicios)
		nuevaRutina.ejercicios.set(listaEjercicios[:randint(4,9)])
		n-=1

	while n>0:		
		shuffle(dias)
		nuevaRutina = modelsRT.Rutina(dia = dias[0])
		shuffle(listaEjercicios)
		nuevaRutina.save()
		nuevaRutina.ejercicios.set(listaEjercicios[:randint(4,9)])
		
		
		
		n-=1
#from scripts import modificarDatos as a
def llenarDatosCliente(n):
	"""
	crea n cantidad de datos de clientes
	los datos son al azar
	no retorna nada
	"""
	while n:
		nuevoDatosCliente = modelsDT.DatosCliente(peso = randint(30,120), altura = randint(140,220))
		nuevoDatosCliente.save()
		n-=1

def llenarUsuario(correo):
	"""
	recibe un correo formato nombre@algo.com
	crea un usuario y retorna el usuario
	"""
	nuevoUser = modelsUS.User(email = correo, password=12345678)
	nuevoUser.save()
	return nuevoUser

def llenarCliente(n):
	"""
	crea n cantidad de clientes

	con datos al azar y rutinas al azar

	por dependencia
	en el proceso crea:
	20 rutinas
	20 datos de cliente
	n cantidad de usuarios

	no retorna nada
	"""
	#n = 0# 
	vaciarBase()
	llenarRutina(20 + n)
	llenarDatosCliente(20 + n)
	listaRaicesCorreos = ["hotmail","icloud","gmail", "itcr", "uned"]
	Vubicacion = "Localización Geografíca"
	listaDatosCliente = list(modelsDT.DatosCliente.objects.all())
	listaRutinas = list(modelsRT.Rutina.objects.all())
	listaRutinaPorDias = [[],[],[],[],[],[],[]]
	letraDia = ["L","K","M","J","V","S","D"]
	rutinasASetear = []

	for rutina in listaRutinas:
		dia = rutina.dia
		indiceDia = letraDia.index(dia)
		listaRutinaPorDias[indiceDia].append(rutina)

	while n:
		rutinasASetear = []
		Vnumero = randint(10000000,99999999)
		shuffle(listaDatosCliente)
		VDatosCliente = listaDatosCliente[0]
		listaDatosCliente = listaDatosCliente[1:]
		Vnombre = names.get_first_name()
		correoUsuario = Vnombre + names.get_last_name() + "@" + listaRaicesCorreos[randint(1,len(listaRaicesCorreos)-1)] + ".com"
		Vuser =  llenarUsuario(correoUsuario)
		nuevoCliente = modelsCL.Cliente(nombre = Vnombre, user = Vuser, numero  = Vnumero, 
			ubicacion = Vubicacion, datos = VDatosCliente)
		nuevoCliente.save()
		shuffle(listaRutinaPorDias[0])
		shuffle(listaRutinaPorDias[1])
		shuffle(listaRutinaPorDias[2])
		shuffle(listaRutinaPorDias[3])
		shuffle(listaRutinaPorDias[4])
		shuffle(listaRutinaPorDias[5])
		shuffle(listaRutinaPorDias[6])
		rutinasASetear.append(listaRutinaPorDias[0][0])
		rutinasASetear.append(listaRutinaPorDias[1][0])
		rutinasASetear.append(listaRutinaPorDias[2][0])
		rutinasASetear.append(listaRutinaPorDias[3][0])
		rutinasASetear.append(listaRutinaPorDias[4][0])
		rutinasASetear.append(listaRutinaPorDias[5][0])
		rutinasASetear.append(listaRutinaPorDias[6][0])
		nuevoCliente.rutinas.set(rutinasASetear)
		n-=1


llenarCliente(30)