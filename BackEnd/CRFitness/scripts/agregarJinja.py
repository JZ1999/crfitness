# -*- coding: utf-8 -*-
import sys
import os


def main():
    """
        Agregar código jinja al archivo dado como
        parámetro en el sys.argv[1], específicamente
        está hecho para editar el inicio.html en el
        la app angular de django para que tenga el código
        necesario para agarrar los scripts y el css necesario
        para el frontend.
    """
    static_files = os.listdir("/home/joseph/Proyects/CRFitness/BackEnd/CRFitness/angular/static/angular/")
    main_js = list(filter(lambda x: "main" in x, static_files))[0]
    polyfills_js = list(filter(lambda x: "polyfills" in x, static_files))[0]
    runtime_js = list(filter(lambda x: "runtime" in x, static_files))[0]
    styles_css = list(filter(lambda x: "styles" in x, static_files))[0]
    _file = sys.argv[1]
    inicioHtml = open(_file, "r")
    out = []
    size = sum(1 for line in open(_file, "r"))
    aux = False
    for line in range(size):
        if (line == 37 and not aux):
            out.append("\n{% csrf_token %}\n{% load staticfiles %}\n{% load static %}\n")
            out.append("<link rel=\"stylesheet\" href=\"{{% static 'angular/{0}' %}}\"></head>\n".format(styles_css))
            inicioHtml.readline()
            aux = True
        elif (line == 77):
            out.append(
                '\n<script type="text/javascript" src="{{% static \'angular/{0}\' %}}"></script><script type="text/javascript" src="{{% static \'angular/{1}\' %}}"></script><script type="text/javascript" src="{{% static \'angular/{2}\' %}}"></script></body>\n'.format(
                    runtime_js, polyfills_js, main_js
                ))
            inicioHtml.readline()
        else:
            out.append(inicioHtml.readline())
    inicioHtml.close()
    inicioHtml = open(_file, "w")
    inicioHtml.write("".join(out))
    inicioHtml.close()



if __name__ == "__main__":
    main()
