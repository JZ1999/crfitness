import matplotlib.pyplot as plt

def grafico(x,y,x_name="x",y_name="y"):
    '''
    genera un grafico a partir de los datos de entrada.

    Parametros: x,y: listas con los datos
                x_name,y_name: nombres de los ejes
    '''

    fig,ax = plt.subplots(1)

    # genera el grafico
    ax.plot(x,y)

    # opciones del grafico
    #fig.suptitle('title', fontsize=12)
    ax.set_xlabel(x_name, fontsize=10)
    ax.set_ylabel(y_name, fontsize=10)

    fig.show()         #muestra el grafico en pantalla
