from .views import foroComentarioViewSet, guardarComentario
from django.urls import path
from rest_framework import routers

router  = routers.SimpleRouter()

router.register('foroComentario',foroComentarioViewSet)

urlpatterns =  router.urls + [
path('guardarComentario', guardarComentario , name='guardarComentario')
]
