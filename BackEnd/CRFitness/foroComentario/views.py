from .serializer import ComentarioSerializer
from .models import ComentarioForm, Comentario
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

class foroComentarioViewSet(viewsets.ModelViewSet):
	queryset = Comentario.objects.all()
	serializer_class = ComentarioSerializer

@csrf_exempt
@api_view(["POST"])
@login_required
def guardarComentario(request):
    '''
    Recibe un POST con los datos y lo guarda en la BD
        id_cliente - id de usuario que crea el comentario
        estrellas - enteros del 1 al 5
        comentario - texto

    '''
    newComentario = ComentarioForm(request.POST)
    if newComentario.is_valid():
        newComentario.save()
        return Response()
    return HttpResponseBadRequest()

# TODO hacer un GET que me devuelva todos los comentarios
# TODO hacer un POST que borre un comentario según su id
