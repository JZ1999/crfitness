from cliente import  models as cliente_models
from django.db import models
from django.forms import ModelForm
import time

class Comentario(models.Model):
    cliente  =  models.ForeignKey(cliente_models.Cliente, on_delete=models.CASCADE)
    ESTRELLAS_CHOISES = [(1,1),(2,2),(3,3),(4,4),(5,5)]
    estrellas = models.IntegerField(choices=ESTRELLAS_CHOISES)
    comentario  = models.TextField(default = ' ')
    fecha = models.DateField(auto_now = True)

    def __str__(self):
        return 'Nombre: {}. Fecha de la publicación: {}.'.format(self.cliente.nombre, self.fecha)

class ComentarioForm(ModelForm):

    class Meta:
        model = Comentario
        fields = '__all__'
