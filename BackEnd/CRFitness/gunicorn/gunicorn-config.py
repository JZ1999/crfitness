import multiprocessing

#           Note que es el virtual env
command = '/home/joseph/.entornos/crfitness/bin/gunicorn'
#           Al proyecto django
pythonpath = '/home/joseph/Proyects/CRFitness/BackEnd/CRFitness'
bind = 'unix:/tmp/gunicorn.sock'
workers = multiprocessing.cpu_count() * 2 + 1
