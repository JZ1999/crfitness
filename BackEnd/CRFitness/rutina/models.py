from django.db import models
from django.forms import ModelForm
from ejercicio import models as modelsEJ
from sortedm2m.fields import SortedManyToManyField


class Rutina(models.Model):
    Dias = [("L", "Lunes"), ("K", "Martes"), ("M", "Miercoles"),
            ("J", "Jueves"), ("V", "Viernes"), ("S", "Sabado"),
            ("D", "Domingo")]
    nombre = models.CharField(max_length=50, default="SIN NOMBRE")
    dia = models.CharField(choices=Dias, default="L", max_length=1)
    ejercicios = SortedManyToManyField(modelsEJ.Ejercicio, blank=True)

    def __str__(self):
        stringEjercicio = self.nombre + " Dia: " + self.dia + " "
        ejerciciosDeRutina = self.ejercicios.all()
        for ejercicio in ejerciciosDeRutina:
            stringEjercicio += ejercicio.nombre + " "

        return stringEjercicio


class RutinaForm(ModelForm):
    class Meta:
        model = Rutina
        fields = '__all__'
