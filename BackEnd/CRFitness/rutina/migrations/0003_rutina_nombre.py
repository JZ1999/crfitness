# Generated by Django 2.1.7 on 2019-06-28 03:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rutina', '0002_auto_20190607_1821'),
    ]

    operations = [
        migrations.AddField(
            model_name='rutina',
            name='nombre',
            field=models.CharField(default='SIN NOMBRE', max_length=50),
        ),
    ]
