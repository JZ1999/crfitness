from .models import Rutina, RutinaForm
from .serializer import RutinaSerializer
from cliente import models as modelsCL
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class RutinaViewSet(viewsets.ModelViewSet):
    queryset = Rutina.objects.all()
    serializer_class = RutinaSerializer


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def guardarRutina(request):
    '''
    Recibe un POST con los datos y lo guarda en la BD

    Request debe contener los siguientes parametros:

    nombre : String

    descripcion : Text

    repeticiones : Integer

    duracion : Float

    descanso : Float


    Ejemplo en diccionario: {'nombre':'Sentadillas',
    'descripcion':'Dobla las piernas','repeticiones':'3', 'duracion':'5.3',
    'descanso':'90.3' }
    '''

    newRutina = RutinaForm(request.POST)
    if newRutina.is_valid():
        newRutina.save()
        return Response()
    return HttpResponseBadRequest()


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def devolverRutinas(request):
    """
    Recibe un POST con el id del cliente que se
    quiere consultar.

    Request debe contener el parametro 'id'.

    Retorna una lista con todas las rutinas
    que el cliente.
    """
    idCl = request.data.get("id", "")
    cliente = get_object_or_404(modelsCL.Cliente, pk=int(idCl))
    rutinasTotal = list(cliente.rutinas.all())  #
    respuesta = {}
    formatoFinalDia = {}
    for rutinas in rutinasTotal:
        dia = rutinas.dia
        listaEjercicios = list(rutinas.ejercicios.all())
        ejerciciosDiccionario = []
        for ejercicio in listaEjercicios:
            nombre = ejercicio.nombre
            descripcion = ejercicio.descripcion
            repeticiones = ejercicio.repeticiones
            series = ejercicio.series
            descanso = ejercicio.descanso
            ejerciciosDiccionario.append(
                {"nombre": nombre, "desc": descripcion, "descanso": descanso, "repeticiones": repeticiones,
                 "series": series})
        formatoFinalDia.update({dia: ejerciciosDiccionario})
    respuesta.update({"rutinas": formatoFinalDia, "nombre": cliente.nombre})
    return JsonResponse(respuesta)
