from .models import Rutina, RutinaForm
from django import forms
from django.contrib import admin

class RutinaAdminForm(forms.ModelForm):
    class Meta:
        model = Rutina
        exclude = ['VencimientoDeRutinas']

class RutinaAdmin(admin.ModelAdmin):
    search_fields = ['dia']

    form = RutinaAdminForm

admin.site.register(Rutina, RutinaAdmin)