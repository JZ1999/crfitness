from .models import Rutina
from rest_framework import serializers


class  RutinaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Rutina 
		fields = '__all__'
		
		