from .views import RutinaViewSet, guardarRutina, devolverRutinas
from django.urls import path
from rest_framework import routers

router  = routers.SimpleRouter()

router.register('rutina',RutinaViewSet)

urlpatterns =  router.urls
urlpatterns += [path('guardarRutina', guardarRutina, name='guardarRutina')] + \
                [path('devolverRutinas', devolverRutinas, name='devolverRutinas')] 
