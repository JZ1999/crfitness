correo package
==============

Subpackages
-----------

.. toctree::

   correo.migrations

Submodules
----------

correo.admin module
-------------------

.. automodule:: correo.admin
   :members:
   :undoc-members:
   :show-inheritance:

correo.apps module
------------------

.. automodule:: correo.apps
   :members:
   :undoc-members:
   :show-inheritance:

correo.models module
--------------------

.. automodule:: correo.models
   :members:
   :undoc-members:
   :show-inheritance:

correo.serializer module
------------------------

.. automodule:: correo.serializer
   :members:
   :undoc-members:
   :show-inheritance:

correo.tests module
-------------------

.. automodule:: correo.tests
   :members:
   :undoc-members:
   :show-inheritance:

correo.urls module
------------------

.. automodule:: correo.urls
   :members:
   :undoc-members:
   :show-inheritance:

correo.views module
-------------------

.. automodule:: correo.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: correo
   :members:
   :undoc-members:
   :show-inheritance:
