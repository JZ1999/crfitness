ejercicio package
=================

Subpackages
-----------

.. toctree::

   ejercicio.migrations

Submodules
----------

ejercicio.admin module
----------------------

.. automodule:: ejercicio.admin
   :members:
   :undoc-members:
   :show-inheritance:

ejercicio.apps module
---------------------

.. automodule:: ejercicio.apps
   :members:
   :undoc-members:
   :show-inheritance:

ejercicio.models module
-----------------------

.. automodule:: ejercicio.models
   :members:
   :undoc-members:
   :show-inheritance:

ejercicio.serializer module
---------------------------

.. automodule:: ejercicio.serializer
   :members:
   :undoc-members:
   :show-inheritance:

ejercicio.tests module
----------------------

.. automodule:: ejercicio.tests
   :members:
   :undoc-members:
   :show-inheritance:

ejercicio.urls module
---------------------

.. automodule:: ejercicio.urls
   :members:
   :undoc-members:
   :show-inheritance:

ejercicio.views module
----------------------

.. automodule:: ejercicio.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: ejercicio
   :members:
   :undoc-members:
   :show-inheritance:
