angular package
===============

Subpackages
-----------

.. toctree::

   angular.migrations

Submodules
----------

angular.admin module
--------------------

.. automodule:: angular.admin
   :members:
   :undoc-members:
   :show-inheritance:

angular.apps module
-------------------

.. automodule:: angular.apps
   :members:
   :undoc-members:
   :show-inheritance:

angular.models module
---------------------

.. automodule:: angular.models
   :members:
   :undoc-members:
   :show-inheritance:

angular.tests module
--------------------

.. automodule:: angular.tests
   :members:
   :undoc-members:
   :show-inheritance:

angular.urls module
-------------------

.. automodule:: angular.urls
   :members:
   :undoc-members:
   :show-inheritance:

angular.views module
--------------------

.. automodule:: angular.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: angular
   :members:
   :undoc-members:
   :show-inheritance:
