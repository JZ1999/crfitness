novedades package
=================

Subpackages
-----------

.. toctree::

   novedades.migrations

Submodules
----------

novedades.admin module
----------------------

.. automodule:: novedades.admin
   :members:
   :undoc-members:
   :show-inheritance:

novedades.apps module
---------------------

.. automodule:: novedades.apps
   :members:
   :undoc-members:
   :show-inheritance:

novedades.models module
-----------------------

.. automodule:: novedades.models
   :members:
   :undoc-members:
   :show-inheritance:

novedades.serializer module
---------------------------

.. automodule:: novedades.serializer
   :members:
   :undoc-members:
   :show-inheritance:

novedades.tests module
----------------------

.. automodule:: novedades.tests
   :members:
   :undoc-members:
   :show-inheritance:

novedades.urls module
---------------------

.. automodule:: novedades.urls
   :members:
   :undoc-members:
   :show-inheritance:

novedades.views module
----------------------

.. automodule:: novedades.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: novedades
   :members:
   :undoc-members:
   :show-inheritance:
