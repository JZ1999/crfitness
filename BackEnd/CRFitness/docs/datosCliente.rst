datosCliente package
====================

Subpackages
-----------

.. toctree::

   datosCliente.migrations

Submodules
----------

datosCliente.admin module
-------------------------

.. automodule:: datosCliente.admin
   :members:
   :undoc-members:
   :show-inheritance:

datosCliente.apps module
------------------------

.. automodule:: datosCliente.apps
   :members:
   :undoc-members:
   :show-inheritance:

datosCliente.models module
--------------------------

.. automodule:: datosCliente.models
   :members:
   :undoc-members:
   :show-inheritance:

datosCliente.serializer module
------------------------------

.. automodule:: datosCliente.serializer
   :members:
   :undoc-members:
   :show-inheritance:

datosCliente.tests module
-------------------------

.. automodule:: datosCliente.tests
   :members:
   :undoc-members:
   :show-inheritance:

datosCliente.urls module
------------------------

.. automodule:: datosCliente.urls
   :members:
   :undoc-members:
   :show-inheritance:

datosCliente.views module
-------------------------

.. automodule:: datosCliente.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: datosCliente
   :members:
   :undoc-members:
   :show-inheritance:
