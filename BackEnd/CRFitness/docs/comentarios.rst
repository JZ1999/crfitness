comentarios package
===================

Subpackages
-----------

.. toctree::

   comentarios.migrations

Submodules
----------

comentarios.admin module
------------------------

.. automodule:: comentarios.admin
   :members:
   :undoc-members:
   :show-inheritance:

comentarios.apps module
-----------------------

.. automodule:: comentarios.apps
   :members:
   :undoc-members:
   :show-inheritance:

comentarios.models module
-------------------------

.. automodule:: comentarios.models
   :members:
   :undoc-members:
   :show-inheritance:

comentarios.serializer module
-----------------------------

.. automodule:: comentarios.serializer
   :members:
   :undoc-members:
   :show-inheritance:

comentarios.tests module
------------------------

.. automodule:: comentarios.tests
   :members:
   :undoc-members:
   :show-inheritance:

comentarios.urls module
-----------------------

.. automodule:: comentarios.urls
   :members:
   :undoc-members:
   :show-inheritance:

comentarios.views module
------------------------

.. automodule:: comentarios.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: comentarios
   :members:
   :undoc-members:
   :show-inheritance:
