cliente package
===============

Subpackages
-----------

.. toctree::

   cliente.management
   cliente.migrations

Submodules
----------

cliente.admin module
--------------------

.. automodule:: cliente.admin
   :members:
   :undoc-members:
   :show-inheritance:

cliente.apps module
-------------------

.. automodule:: cliente.apps
   :members:
   :undoc-members:
   :show-inheritance:

cliente.models module
---------------------

.. automodule:: cliente.models
   :members:
   :undoc-members:
   :show-inheritance:

cliente.serializer module
-------------------------

.. automodule:: cliente.serializer
   :members:
   :undoc-members:
   :show-inheritance:

cliente.tasks module
--------------------

.. automodule:: cliente.tasks
   :members:
   :undoc-members:
   :show-inheritance:

cliente.tests module
--------------------

.. automodule:: cliente.tests
   :members:
   :undoc-members:
   :show-inheritance:

cliente.urls module
-------------------

.. automodule:: cliente.urls
   :members:
   :undoc-members:
   :show-inheritance:

cliente.views module
--------------------

.. automodule:: cliente.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: cliente
   :members:
   :undoc-members:
   :show-inheritance:
