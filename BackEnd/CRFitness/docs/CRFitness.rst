CRFitness package
=================

Submodules
----------

CRFitness.celery module
-----------------------

.. automodule:: CRFitness.celery
   :members:
   :undoc-members:
   :show-inheritance:

CRFitness.settings module
-------------------------

.. automodule:: CRFitness.settings
   :members:
   :undoc-members:
   :show-inheritance:

CRFitness.urls module
---------------------

.. automodule:: CRFitness.urls
   :members:
   :undoc-members:
   :show-inheritance:

CRFitness.wsgi module
---------------------

.. automodule:: CRFitness.wsgi
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: CRFitness
   :members:
   :undoc-members:
   :show-inheritance:
