angular.migrations package
==========================

Module contents
---------------

.. automodule:: angular.migrations
   :members:
   :undoc-members:
   :show-inheritance:
