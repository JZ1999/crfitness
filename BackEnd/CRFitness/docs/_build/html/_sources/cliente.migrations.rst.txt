cliente.migrations package
==========================

Submodules
----------

cliente.migrations.0001\_initial module
---------------------------------------

.. automodule:: cliente.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

cliente.migrations.0002\_auto\_20190608\_1946 module
----------------------------------------------------

.. automodule:: cliente.migrations.0002_auto_20190608_1946
   :members:
   :undoc-members:
   :show-inheritance:

cliente.migrations.0003\_auto\_20190616\_0149 module
----------------------------------------------------

.. automodule:: cliente.migrations.0003_auto_20190616_0149
   :members:
   :undoc-members:
   :show-inheritance:

cliente.migrations.0004\_auto\_20190622\_1244 module
----------------------------------------------------

.. automodule:: cliente.migrations.0004_auto_20190622_1244
   :members:
   :undoc-members:
   :show-inheritance:

cliente.migrations.0005\_auto\_20190626\_0941 module
----------------------------------------------------

.. automodule:: cliente.migrations.0005_auto_20190626_0941
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: cliente.migrations
   :members:
   :undoc-members:
   :show-inheritance:
