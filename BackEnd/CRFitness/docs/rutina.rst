rutina package
==============

Subpackages
-----------

.. toctree::

   rutina.migrations

Submodules
----------

rutina.admin module
-------------------

.. automodule:: rutina.admin
   :members:
   :undoc-members:
   :show-inheritance:

rutina.apps module
------------------

.. automodule:: rutina.apps
   :members:
   :undoc-members:
   :show-inheritance:

rutina.models module
--------------------

.. automodule:: rutina.models
   :members:
   :undoc-members:
   :show-inheritance:

rutina.serializer module
------------------------

.. automodule:: rutina.serializer
   :members:
   :undoc-members:
   :show-inheritance:

rutina.tests module
-------------------

.. automodule:: rutina.tests
   :members:
   :undoc-members:
   :show-inheritance:

rutina.urls module
------------------

.. automodule:: rutina.urls
   :members:
   :undoc-members:
   :show-inheritance:

rutina.views module
-------------------

.. automodule:: rutina.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: rutina
   :members:
   :undoc-members:
   :show-inheritance:
