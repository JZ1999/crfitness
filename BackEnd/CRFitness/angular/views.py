from django.shortcuts import render, HttpResponse, redirect

def home(request):
    return render(request, 'angular/index.html')

def redirect_view(request):
    response = redirect('/')
    return response

def handler404(request, *args, **argv):
    return render( request, 'angular/index.html', {'url': request.META['PATH_INFO']})
