from .models import Cliente
from .serializer import ClienteSerializer, LoginSerializer
from django.conf import settings
from django.contrib.auth import login as django_login, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Usuario import models as modelsUS


class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        django_login(request, user)
        token, created = Token.objects.get_or_create(user=user)
        response = {"token": token.key}
        return JsonResponse(response)


class LogoutView(APIView):
    authentication_classes = (TokenAuthentication,)

    def post(self, request):
        request.user.auth_token.delete()
        django_logout(request)
        return Response(status=204)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def logout(request):
    Token.objects.get(user=request.user).delete()
    return Response(status=204)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@login_required
def getInfoCliente(request):
    '''
    Recibe POST con un usuario.

    Retorna json con toda la información del usuario.
    '''
    cliente = get_object_or_404(Cliente, user=request.user)
    json_response = model_to_dict(cliente)
    json_response["foto"] = settings.MEDIA_URL + str(json_response["foto"])
    del json_response["rutinas"]

    return JsonResponse(json_response)


@api_view(['GET'])
def administradores(request):
    '''
    Metodo Get que retorna todos los usuarios que pertenezcan al grupo
    Recepción.
    '''
    administradores = []
    usuarios = modelsUS.User.objects.all()
    for usuario in usuarios:
        if usuario.groups.filter(name="Recepcion").exists():  # ubicar los user que sean de Recepción
            email_user = usuario.email
            try:
                clienteUsuario = Cliente.objects.get(user=usuario)
                nombre = clienteUsuario.nombre
                foto = clienteUsuario.foto.url
                administradores.append({"email": email_user, "name": nombre, "photo": foto})
            except Exception:
                pass

    return JsonResponse({"administradores": administradores})

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def cambiarContraseña(request):
    '''
    Recibe un POST para cambiar la contraseña de un usuario.

    Verifica que la contraseña anterior sea la correcta, en ese caso cambia el
    valor de la contraseña del usuario.

    El request debe contener los siguientes parametros:

    contra_nueva : String

    contra_vieja : String

    Ejemplo en diccionario {'contra_vieja':'password1',
    'contra_nueva':'password2'}

    Retorna JSON con información sobre el exito de la operación.
    '''
    usuario = request.user
    contraNueva = request.data.get("contra_nueva", None)
    contraVieja = request.data.get("contra_vieja", None)
    respuesta = {"respuesta": ""}
    if not contraNueva or not contraVieja or not usuario:
        respuesta["respuesta"] = "No enviaste toda la información necesaria para cambiar tu contraseña."
    elif not usuario.check_password(contraVieja):
        respuesta["respuesta"] = "La contraseña que ingresaste está incorrecta"
    else:
        usuario.set_password(contraNueva)
        usuario.save()
        respuesta["respuesta"] = "La contraseña se ha cambiado."
    return JsonResponse(respuesta)
