from comentarios import models as modelsCM
from dateutil.relativedelta import relativedelta
from datosCliente import models as modelsDT
from django.core.validators import RegexValidator
from django.db import models
from rutina import models as modelsRT
from sortedm2m.fields import SortedManyToManyField
from Usuario.models import User
import datetime

class Cliente(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, default = 0)
    nombre = models.CharField(max_length=240, blank=False)
    phone_regex = RegexValidator(regex=r'^\d{9,15}$', message="Número debe estar en el formato de : '+999999999'. Hasta 15 dígitos están permitidos.")
    numero = models.CharField(max_length=17, validators=[phone_regex], blank=True, null=True)
    ubicacion  = models.TextField(default = ' ', blank=True)
    datos = models.OneToOneField(modelsDT.DatosCliente, on_delete = models.SET_NULL, null=True ,blank=True)
    rutinas = SortedManyToManyField(modelsRT.Rutina, blank = True)
    foto = models.ImageField(upload_to = 'cliente' , default= "profile_default.png")
    VencimientoDeRutinas = models.DateField(null=True)

    #Variable unicamente utilizada para detectar un cambio en rutinas
    _rutina_original = None

    def get_rutinas(self):
        return ",".join([str(r) for r in self.rutinas.all()])

    def __init__(self, *args, **kwargs):
        '''
        Guarda el valor de rutinas en rutina original antes de que se realice
        cualquier accion sobre el objeto cliente.
        '''
        super(Cliente, self).__init__(*args, **kwargs)
        self._rutina_original = self.get_rutinas()

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        '''
        Actualiza la fecha de Vencimiento cada vez que rutinas cambia.
        '''
        if self.rutinas != self._rutina_original:
            self.VencimientoDeRutinas = datetime.date.today() + relativedelta(months=+1)
        super(Cliente, self).save(force_insert, force_update, *args, **kwargs)
        self._rutina_original = self.rutinas


    class Meta:
        verbose_name_plural = "clientes"

    def __str__(self):
        return 'Nombre: {}. Email: {}.'.format(self.nombre, self.user.email)

