from __future__ import absolute_import, unicode_literals
from celery import task
from cliente.models import Cliente
from dateutil.relativedelta import relativedelta
from django.http import HttpResponse
import datetime

@task()
def correoDeVencimientoDeRutinas():
    '''
    Envia un correo avisando si faltan al menos 2 dias antes de que se cumpla
    la fecha de Vencimiento de la rutina de un cliente.
    '''

    #Correos para 2 dias antes
    fechaDeVencimiento = datetime.date.today() + relativedelta(days =+ 2)
    clientes = Cliente.objects.filter(VencimientoDeRutinas = fechaDeVencimiento).values()
    for cliente in clientes:
        print(cliente, cliente.VencimientoDeRutinas)
        ##ACA IR ENVIAR CORREO de 2 dias

    #Correos para 1 dia antes
    fechaDeVencimiento = datetime.date.today() + relativedelta(days =+ 1)
    clientes = Cliente.objects.filter(VencimientoDeRutinas = fechaDeVencimiento).values()
    for cliente in clientes:
        print(cliente, cliente.VencimientoDeRutinas)
        ##ACA IR ENVIAR CORREO de 1 dias

    fechaDeVencimiento = datetime.date.today()
    clientes = Cliente.objects.filter(VencimientoDeRutinas = fechaDeVencimiento).values()
    for cliente in clientes:
        print(cliente, cliente.VencimientoDeRutinas)
        ##ACA IR ENVIAR CORREO de 2 dias

@task()
def VencimientoDeRutinas():
    """
    Elimina todas las rutinas que vencen el dia de la ejecucion
    """
    fechaDeVencimiento = datetime.date.today() + relativedelta(days =- 2)
    clientes = Cliente.objects.filter(VencimientoDeRutinas=fechaDeVencimiento).values()
    for cliente in clientes:
        cliente_modificar = Cliente.objects.get(user_id = cliente['user_id'])
        cliente_modificar.rutinas.clear()
        cliente_modificar.save()
        ##ACA IR ENVIAR CORREO de 0 dias
