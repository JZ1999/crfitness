from .views import ClienteViewSet, LoginView , getInfoCliente,cambiarContraseña, administradores, logout
from django.urls import path
from rest_framework import routers


router = routers.SimpleRouter()

router.register('cliente', ClienteViewSet)

urlpatterns = router.urls + [
    path('auth/login', LoginView.as_view()),
    path('auth/logout', logout),
    path('cliente/token', getInfoCliente),
    path('administradores/', administradores),
    path('cambiarcontra/', cambiarContraseña),
]
