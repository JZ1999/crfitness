from django.contrib import admin
from .models import Cliente
from django import forms


# Register your models here.
class ClienteAdminForm(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = "__all__"


class ClienteAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    ordering = ['nombre']

    form = ClienteAdminForm
    readonly_fields = ["VencimientoDeRutinas"]

    def get_form(self, request, obj=None, **kwargs):
        if request.user.groups.filter(name="Recepcion").exists():
            setattr(self, "exclude", ("datos", "numero", "rutinas", "VencimientoDeRutinas"))
        elif request.user.groups.filter(name="Entrenador").exists():
            setattr(self, "exclude", ("numero", "ubicacion"))
            self.readonly_fields += ["user"]
        return super().get_form(request, obj, **kwargs)


admin.site.register(Cliente, ClienteAdmin)

