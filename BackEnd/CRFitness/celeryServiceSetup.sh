#!/bin/bash

#Installando supervisor
sudo apt-get install supervisor

#creacion de configuraciones
cat scripts/CRFitnessWorker.txt > /etc/supervisor/conf.d/celery_CRFitness_worker.conf
cat scripts/CRFitnessbeat.txt > /etc/supervisor/conf.d/celery_CRFitness_beat.conf

#creacion de loggers
mkdir /var/log/celery
sudo touch /var/log/celery/CRFitness_worker.log
sudo touch /var/log/celery/CRFitness_beat.log

#Actualizando cambios
sudo supervisorctl reread
sudo supervisorctl update

#Iniciando Los Procesos
sudo supervisorctl start CRFitnessWorker
sudo supervisorctl start CRFitnessbeat
