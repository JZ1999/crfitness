#!/bin/bash
#Este script debe ser ejecutado como sudo

#Carpeta almacenadora de backups
sudo mkdir $PWD/backups

#Creacion del script
sudo echo "sudo -u $DB_USUARIO pg_dump -U $DB_USUARIO $DB_NOMBRE > $PWD/backups/$DB_NOMBRE.bak" > $PWD/backupscript.sh
sudo chgrp sudo $PWD/backupscript.sh
sudo chmod 750 $PWD/backupscript.sh

#Automatizacion con Cron
sudo crontab -l > /tmp/my.cron
sudo echo "0 2 10 * * $PWD/backupscript.sh" >> /tmp/my.cron
sudo crontab < /tmp/my.cron
sudo rm /tmp/my.cron
