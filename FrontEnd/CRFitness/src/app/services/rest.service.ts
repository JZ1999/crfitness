import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Cliente } from '../models/cliente';
import { Rutina } from '../models/rutina';


//  const api = 'http://localhost:8000/api/v1.0';
const cookies = document.cookie.split(';');
let csrf_cookie = '';
cookies.forEach((data) => {
    const token = data.split('=');
    if (token[0] === 'csrftoken') {
        csrf_cookie = token[1];
    }
});

const api = '/api/v1.0';
let httpOptions = {
  headers: new HttpHeaders({ 'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': [
        api,
        ],
        'X-CSRFToken': csrf_cookie,
        'Authorization': '',
        // Estos headers son por seguridad
        // TODO
        // Hacer que estos headers apliquen solo
        // a informacion confidencial no a cualquiera XHR
        // sino performance bajará
        'cache-control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': '-1',

  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {
    errors = '';

    constructor(private http: HttpClient) { }

    private extractData(res: Response) {
      const body = res;
      return body || { };
    }

    //  getJoke(amount: Number): Observable<any> {
    //      const uri = "imagen/?format=json";
    //      return this.http.get(api+uri, httpOptions).pipe(
    //      map(this.extractData));
    //  }


    getAccount(_id: Number): Observable<any> {
        //  let cookies = document.cookie.split(";");
        //  let csrf_cookie = ""
        //  cookies.forEach((data) => {
        //      let token=data.split("=");
        //      if(token[0]=="csrftoken"){
        //          csrf_cookie = token[1];
        //      };
        //  });
        const uri = '/cliente/login/';
        const info = {id: _id, csrfmiddlewaretoken: csrf_cookie};
        return this.http.post<Cliente>(api + uri, info, httpOptions).pipe(
        map(this.extractData));
    }

    public updateCliente(customer: Cliente) {}

    public getClienteById(_id: number): Observable<Cliente> {
        const uri = '/cliente/' + _id.toString();
        const info = {csrfmiddlewaretoken: csrf_cookie};
        return this.http.post<Cliente>(api + uri, info, httpOptions).pipe(
        map(this.extractData));

    }

    public getClienteByToken(_token: string): Observable<Cliente> {
    const uri = '/cliente/token';
    const info = {csrfmiddlewaretoken: csrf_cookie};
    let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type':  'application/json',
          'Access-Control-Allow-Origin': [
            api,
          ],
          'X-CSRFToken': csrf_cookie,
          'Authorization': 'Token ' + _token,
          'cache-control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': '-1',

        })
    };
    return this.http.post<Cliente>(api + uri, info, httpOptions).pipe(
      map(this.extractData));

    }

    public getClientes(url?: string) {}


    public getCliente(correo: string, pwd: string): Observable<any> {
        const uri = '/auth/login';
        const info = {username: correo, password: pwd, csrfmiddlewaretoken: csrf_cookie};
        return this.http.post<Cliente>(api + uri, info, httpOptions).pipe(
        map(this.extractData));
              //  return new Cliente(1, "Joseph Zamora", "joseph.zamora64@yahoo.com", "84599023");
    }


    public getRutina(id: string, _token: string): any {
        const uri = '/devolverRutinas';
        const info = {id: id, csrfmiddlewaretoken: csrf_cookie};
        let httpOptions = {
            headers: new HttpHeaders({ 'Content-Type':  'application/json',
              'Access-Control-Allow-Origin': [
                api,
              ],
              'X-CSRFToken': csrf_cookie,
              'Authorization': 'Token ' + _token,
              'cache-control': 'no-cache, no-store, must-revalidate',
              'Pragma': 'no-cache',
              'Expires': '-1',

            })
        };
        return this.http.post<Rutina>(api + uri, info, httpOptions).pipe(
        map(this.extractData));
    }


    public setComentarios(_cliente: number, _estrellas: number, _comentario: string): any {
        const uri = '/guardarComentario';
        const info = {cliente: _cliente, estrellas: _estrellas, comentario: _comentario,
                     csrfmiddlewaretoken: csrf_cookie};
        return this.http.post(api + uri, info, httpOptions).pipe(
        map(this.extractData));
    }


  public getAdministradores(): Observable<any> {
    const uri = '/administradores/';
    return this.http.get(api + uri, httpOptions).pipe(
      map(this.extractData));
  }

    public enviarCorreo(_correo: string, _nombre: string, _msj: string, _adminName: string, _adminEmail: string): any {
        const uri = '/enviarCorreo';
        const info = {correo: _correo, nombre: _nombre, mensaje: _msj,
            adminName: _adminName, adminEmail: _adminEmail, csrfmiddlewaretoken: csrf_cookie};
        console.log(info);
        return this.http.post(api + uri, info, httpOptions).pipe(
        map(this.extractData));
    }

    public logout(_token: string) {
      const uri = '/auth/logout';
      let httpOptions = {
        headers: new HttpHeaders({ 'Content-Type':  'application/json',
          'Access-Control-Allow-Origin': [
            api,
          ],
          'X-CSRFToken': csrf_cookie,
          'Authorization': 'Token ' + _token,
          'cache-control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': '-1',

        })
      };
      const info = {csrfmiddlewaretoken: csrf_cookie};
      return this.http.post(api + uri, info, httpOptions).pipe(
        map(this.extractData));
    }
      public cambiar_contra(_token: string, contra_nueva: string, contra_vieja: string) {
        const uri = '/cambiarcontra/';
        let httpOptions = {
          headers: new HttpHeaders({ 'Content-Type':  'application/json',
            'Access-Control-Allow-Origin': [
              api,
            ],
            'X-CSRFToken': csrf_cookie,
            'Authorization': 'Token ' + _token,
            'cache-control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '-1',

          })
        };
        const info = {csrfmiddlewaretoken: csrf_cookie, contra_vieja: contra_vieja, contra_nueva: contra_nueva};
        return this.http.post(api + uri, info, httpOptions).pipe(
          map(this.extractData));
      }

}
