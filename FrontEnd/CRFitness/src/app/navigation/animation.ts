export function navAnim(){
    //@ts-ignore
    $('#nav').hover(()=> {
    //@ts-ignore
        $('.menu').css('pointer-events','auto');
    });
    //@ts-ignore
    var $els = $('.menu a, .menu header');
    var count = $els.length;
    var grouplength = Math.ceil(count/3);
    var groupNumber = 0;
    var i = 1;
    //@ts-ignore
    $('.menu').css('--count',count+'');
    $els.each(function(j){
        if ( i > grouplength ) {
            groupNumber++;
            i=1;
        }
    //@ts-ignore
        $(this).attr('data-group',groupNumber);
        i++;
    });

    //@ts-ignore
    $('.menu footer button').on('click',function(e){
    //@ts-ignore
        e.preventDefault();
        $els.each(function(j){
    //@ts-ignore
            $(this).css('--top',$(this)[0].getBoundingClientRect().top + ($(this).attr('data-group') * -15) - 20);
    //@ts-ignore
            $(this).css('--delay-in',j*.1+'s');
    //@ts-ignore
            $(this).css('--delay-out',(count-j)*.1+'s');
        });
    //@ts-ignore
        $('.menu').toggleClass('closed');
        e.stopPropagation();
        setTimeout(function(){

            //@ts-ignore
            $('.menu').css('pointer-events','none');
        }, 1000);
    });

    // run animation once at beginning for demo
    setTimeout(function(){
        document.getElementById("menu").style.zIndex = "0";
    //@ts-ignore
        $('.menu footer button').click();
        //  setTimeout(function(){
    //@ts-ignore
            //  $('.menu footer button').click();
        //  }, (count * 100) + 500 );
    }, 1000);
}

export function toggleAnimation(){
			//@ts-ignore
            $('.menu footer button').click();
}
