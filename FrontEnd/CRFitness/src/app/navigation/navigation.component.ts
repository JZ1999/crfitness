import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {RestService} from '../services/rest.service';
import {Router} from '@angular/router';
import {globals} from '../globals';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.less']
})
export class NavigationComponent implements OnInit {
  validPassword = false;
  validEmail = false;
  submitted = false;
  formulario = new FormGroup({
    formEmail: new FormControl(),
    formPwd: new FormControl(),

  });
  cliente;

  rutina_or_login() {
    console.log(sessionStorage);
    const token = sessionStorage.getItem('token');
    if (token !== undefined && token != null) {
      this.api.getClienteByToken(token).subscribe(data => {
          this.cliente = data;
        },
        error => console.error(error));
    }
    if (token !== null) {
      const url = 'rutina';
      this.router.navigateByUrl(url);
    } else {
      const url = 'iniciar';
      this.router.navigateByUrl(url);
    }
  }

  constructor(private formBuilder: FormBuilder, private api: RestService, private router: Router) {
    this.createForm();
    const token = sessionStorage.getItem('token');
    if (token !== undefined && token != null) {
      this.api.getClienteByToken(token).subscribe(data => {
          this.cliente = data;
        },
        error => console.error(error));
    }
  }


  ngOnInit() {
  }

  createForm() {
    this.formulario = this.formBuilder.group({
      formEmail: ['', [Validators.required, Validators.email],
        this.validEmail],
      formPwd: ['', [Validators.required, Validators.minLength(6)],
        this.validPassword]
    });
  }

  get f() {
    return this.formulario.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.formulario.invalid) {
      return;
    }

    this.api.getCliente(this.formulario.controls['formEmail'].value,
      this.formulario.controls['formPwd'].value,
    ).subscribe(data => {
        sessionStorage.setItem('token', data.token);
        //@ts-ignore
        document.getElementById('formulario').reset();
        location.reload();
      },
      error => {
        console.error(error);
        alert('Correo o contraseña incorrecta.');
      }
    );


  }

  logout() {
    this.api.logout(sessionStorage.getItem('token')).subscribe(data => {
      console.log('Logout');
      console.log(data);
    }, error => console.error(error));
    sessionStorage.removeItem('token');
    location.href = '/';
  }

}
