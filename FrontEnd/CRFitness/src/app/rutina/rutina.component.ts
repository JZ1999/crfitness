import { Component, OnInit } from '@angular/core';
import {RestService} from '../services/rest.service';
import {Cliente} from '../models/cliente';
import {Rutina} from '../models/rutina';

@Component({
  selector: 'app-rutina',
  templateUrl: './rutina.component.html',
  styleUrls: ['./rutina.component.less']
})
export class RutinaComponent implements OnInit {

    image = "https://image.ibb.co/jw55Ex/def_face.jpg";
    cliente: Cliente;
    name = "cliente";
    greet = "Buenas tardes,";
    id: string;
    token: string;

    rutinas;
    currRutina;


  constructor(private rest: RestService){
    this.token = sessionStorage.getItem("token");
    //@ts-ignore
    rest.getClienteByToken(this.token).subscribe( data => {
        //@ts-ignore
        data.id = data.user;
        this.cliente = data;
        this.name = this.cliente.nombre;
        if(this.cliente.foto){
            this.image = this.cliente.foto;
        }

        rest.getRutina(this.cliente.id, this.token).subscribe(data => {
                this.rutinas = data.rutinas;
                console.log(this.rutinas);
            },
            error => {
                console.error(error);
            }
        );
    },
        error => console.error(error)
    );
    const date = new Date();
    const hours = date.getHours();

    if (hours < 12) {
        this.greet = "Buenos días,";
    } else if (hours < 18) {
        this.greet = "Buenas tardes,";
    } else {
        this.greet = "Buenas noches,";
    }

  }

  ngOnInit() {
  }

  getRutina(event){
    let day_id = event.srcElement.attributes.id.nodeValue;
    let input_tags = document.getElementsByTagName("input")
    if( day_id == "weekday-mon" || day_id == "weekday-mon2"){
        this.currRutina = this.rutinas.L;
    }
    else if( day_id == "weekday-tue" || day_id == "weekday-tue2"){
        this.currRutina = this.rutinas.K;
    }
    else if( day_id == "weekday-wed" || day_id == "weekday-wed2"){
        this.currRutina = this.rutinas.M;
    }
    else if( day_id == "weekday-thu" || day_id == "weekday-thu2"){
        this.currRutina = this.rutinas.J;
    }
    else if( day_id == "weekday-fri" || day_id == "weekday-fri2"){
        this.currRutina = this.rutinas.V;
    }
    else if( day_id == "weekday-sat" || day_id == "weekday-sat2"){
        this.currRutina = this.rutinas.S;
    }
    else if( day_id == "weekday-sun" || day_id == "weekday-sun2"){
        this.currRutina = this.rutinas.D;
    }
    let quitar_rutina_del_gui = false;
    //@ts-ignore
    for(let input of input_tags){
        if(input.id != day_id){
            input.checked = false;
        }
        quitar_rutina_del_gui = quitar_rutina_del_gui || input.checked;
    }
    if(!quitar_rutina_del_gui){
        this.currRutina = null;
    }
  }

}
