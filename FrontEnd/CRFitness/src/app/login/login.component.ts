import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {RestService} from '../services/rest.service';
import {Router} from '@angular/router';
import {globals} from '../globals';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  validPassword = false;
  validEmail = false;
  submitted = false;
  formulario = new FormGroup({
    formEmail: new FormControl(),
    formPwd: new FormControl(),

  });

  constructor(private formBuilder: FormBuilder, private api: RestService,
              private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.formulario = this.formBuilder.group({
      formEmail: ['', [Validators.required, Validators.email],
        this.validEmail],
      formPwd: ['', [Validators.required, Validators.minLength(6)],
        this.validPassword]
    });
  }

  get f() {
    return this.formulario.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.formulario.invalid) {
      return;
    }

    this.api.getCliente(this.formulario.controls['formEmail'].value,
      this.formulario.controls['formPwd'].value,
    ).subscribe(data => {
        sessionStorage.setItem('token', data.token);
        // @ts-ignore
        document.getElementById('formulario2').reset();
        location.pathname = '/';
      },
      error => console.error(error)
    );
  }
}
