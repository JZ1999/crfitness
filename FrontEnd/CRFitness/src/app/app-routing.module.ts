import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IntroComponent} from './intro/intro.component';
import {ContactComponent} from './contact/contact.component';
import {AboutComponent} from './about/about.component';
import {ProfileComponent} from './profile/profile.component';
import {LoginComponent} from './login/login.component';
import {RutinaComponent} from './rutina/rutina.component';

const routes: Routes = [
  { path: '', component: IntroComponent},
  { path: 'sobre', component: AboutComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'perfil', component: ProfileComponent},
  { path: 'rutina', component: RutinaComponent},
  { path: 'iniciar', component: LoginComponent},
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule { }
