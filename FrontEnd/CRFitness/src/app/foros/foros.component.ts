import { Component, OnInit } from '@angular/core';
import {RestService} from '../services/rest.service';

@Component({
  selector: 'app-foros',
  templateUrl: './foros.component.html',
  styleUrls: ['./foros.component.less']
})
export class ForosComponent implements OnInit {
    cliente_id: number;
    msg: string;
    estrellas: number;
  constructor(public rest: RestService) { 
    this.cliente_id = parseInt(sessionStorage.getItem("id"));
  }

  ngOnInit() {
  }

  addComment(){
    this.rest.setComentarios(this.cliente_id, this.estrellas, this.msg).subscribe( data =>
    {
      console.log(data);
    },
    error => console.error(error)
    );
  }
  setStars(event){
      this.estrellas = parseInt(event.target.value);
  }
    status: boolean = false;
    clickEvent(){
        this.status = !this.status;       
    }
}
