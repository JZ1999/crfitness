import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {RestService} from '../services/rest.service';
import {Router} from '@angular/router';
import {globals} from '../globals';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.less']
})
export class ContactComponent implements OnInit {

    nombre: string;
    correo: string;
    mensaje: string;
    validCorreo = false;
    validNombre = false;
    validMensaje = false;
    submitted = false;
    admins = [];
    adminEmail = 'crfitnesssrl@gmail.com';
    adminName: string;

  constructor(private api: RestService) {
    api.getAdministradores().subscribe(data => {
        this.admins = data['administradores'];
    });
  }

  ngOnInit() {
  }

/**
   * Process the form we have. Send to whatever backend
   * Only alerting for now
   */
  submit(): any {
    this.submitted = true;

    return this.api.enviarCorreo(
      this.correo,
      this.nombre,
      this.mensaje,
      this.adminName,
      this.adminEmail
    );
  }

  processForm() {
      let error_msg;
      // @ts-ignore
      $('#error').empty();
      // @ts-ignore
      $('#error').removeClass('d-none');
      if (!this.validCorreo) {
            error_msg = '<p>Formato de correo incorrecto</p>';
      } else if (!this.validNombre) {
            error_msg = '<p>Formato del nombre incorrecto</p>';
      } else if (!this.validMensaje) {
            error_msg = '<p>Formato del mensaje incorrecto</p>';
      } else {
        // @ts-ignore
        $('#error').addClass('d-none');
        this.submit().subscribe(data => {
                // @ts-ignore
                bootbox.alert({
                    message: '¡Mensaje enviado con éxito!',
                    className: 'rubberBand animated'
                });
              console.log(data['response']);
          },
          error => {
              console.log(error);
              // @ts-ignore
              bootbox.alert({
                  message: 'Hubo un error con el servidor',
                  className: 'flash animated'
              });
          }
          );
        return;
      }
        // @ts-ignore
        $('#error').append(error);
  }

  selectAdmin(email: string, name: string) {
    console.log(email, name);
    this.adminEmail = email;
    this.adminName = name;
  }

    onChangeCorreo(newValue) {
      // tslint:disable-next-line:max-line-length
        const validEmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.validCorreo = validEmailRegEx.test(newValue);

      }


    onChangeNombre(newValue) {
        const validNombreRegEx = /\w/;
        this.validNombre = validNombreRegEx.test(newValue);

      }


    onChangeMensaje(newValue) {
        const validMensajeRegEx = /\w+/;
        this.validMensaje = validMensajeRegEx.test(newValue);

      }

}
