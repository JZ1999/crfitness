import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formateoapellido' })
export class formateoApellido implements PipeTransform {
  transform(nombre: string) {
    const primerNombre = nombre.split(" ")[0];
    return nombre.slice(primerNombre.length).trim();
  }
}


