
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formateonombre' })
export class formateoNombre implements PipeTransform {
  transform(nombre: string) {
    const primerNombre = nombre.split(" ")[0];
    return nombre.slice(0,primerNombre.length).trim();
  }
}

