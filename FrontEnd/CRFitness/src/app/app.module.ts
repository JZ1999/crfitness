import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigationComponent } from './navigation/navigation.component';
import { IntroComponent } from './intro/intro.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { ProfileComponent } from './profile/profile.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import {formateoNombre} from './pipes/formateoNombre.pipe';
import {formateoApellido} from './pipes/formateoApellido.pipe';
import { LoginComponent } from './login/login.component';
import { RutinaComponent } from './rutina/rutina.component';
import { ForosComponent } from './foros/foros.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    IntroComponent,
    ContactComponent,
    AboutComponent,
    ProfileComponent,
    ForbiddenComponent,
    formateoNombre,
	formateoApellido,
	LoginComponent,
	RutinaComponent,
	ForosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
