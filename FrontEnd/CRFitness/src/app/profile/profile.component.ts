import {Component, OnInit} from '@angular/core';
import {Cliente} from '../models/cliente';
import {globals} from '../globals';
import {RestService} from '../services/rest.service';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  cliente: Cliente;
  id: string;
  validNombre = false;
  validCorreo = false;
  validUbicacion = false;
  validNumero = false;
  validForm = false;
  validPassword = false;
  validPasswordOld = false;
  validConfirmPassword = false;
  validPasswordForm = false;
  nombre: string;
  correo: string;
  ubicacion: string;
  numero: string;
  token: string;
  current_password: string;
  new_password: string;
  new_password_confirm: string;

  constructor(public rest: RestService, private api: RestService) {
  }

  ngOnInit() {
    this.token = sessionStorage.getItem('token');
    this.rest.getClienteByToken(this.token).subscribe((data: Cliente) => {
        this.cliente = data;
      },
      error => console.log(error)
    );
  }


  processForm(): boolean {

    let validRegEx = /[\w|\s]*/;
    this.validNombre = validRegEx.test(this.nombre);

    validRegEx = /[\d]*/;
    this.validNumero = validRegEx.test(this.numero);

    validRegEx = /(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)/;
    this.validCorreo = validRegEx.test(this.correo);

    validRegEx = /\b((?!=|\,|\.).)+(.)\b/;
    this.validUbicacion = validRegEx.test(this.ubicacion);

    console.log("correo " + this.validCorreo + " " + this.correo);
    console.log("numero " + this.validNumero + " " + this.correo);
    console.log("nombre " + this.validNombre + " " + this.correo);
    console.log("ubicacion " + this.validUbicacion + " " + this.correo);

    return this.validNombre && this.validCorreo && this.validNumero && this.validUbicacion;

  }


  onChangeCorreo(newValue) {
    const validEmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (validEmailRegEx.test(newValue)) {
      this.validCorreo = true;
    } else {
      this.validCorreo = false;
    }
    console.log("valcor " + this.validCorreo);

  }

  onChangeNuevaContra(newValue) {
    const validPasswordRegEx = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$/;
    this.validPassword = validPasswordRegEx.test(newValue);
    this.validPasswordForm = this.validPassword && this.validConfirmPassword && this.validPasswordOld;
  }

  onChangeContraConfirm(newValue) {
    //@ts-ignore
    this.validConfirmPassword = newValue === document.getElementById('new_password').value;
    this.validPasswordForm = this.validPassword && this.validConfirmPassword && this.validPasswordOld;
  }

  onChangeContra(newValue) {
    const validPasswordRegEx = /\w+/;
    this.validPasswordOld = validPasswordRegEx.test(newValue);
    this.validPasswordForm = this.validPassword && this.validConfirmPassword && this.validPasswordOld;
  }

  onSubmit() {
    this.validForm = this.processForm();
  }

  password_reset() {
    if (!this.validPasswordOld){
      alert('Debes poner tu contraseña actual');
    } else if (!this.validConfirmPassword) {
      alert('Las contraseñas no coinciden.');
    } else if (!this.validPassword) {
      alert('La contraseña debe tener de 6 a 15 caracteres incluyendo numeros y letras');
    } else {
      this.api.cambiar_contra(this.token, this.new_password, this.current_password).subscribe((data) => {
        alert("La contraseña se cambió con éxito");
      }, error => {
        console.error(error);
        alert("Hubo un problema con el servidor. Intenta de nuevo luego.");
      });
    }
  }

}
