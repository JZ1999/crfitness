export class Cliente {
    constructor(_id: string, _nombre: string,
               _correo: string,
               _ubicacion: string,
               _foto: string,
               _numero: string) {
            this.id = _id;
            this.nombre = _nombre;
            this.correo = _correo;
            this.numero = _numero;
            this.ubicacion = _ubicacion;
            this.foto = _foto;
    }
    id: string;
    nombre: string;
    correo: string;
    numero: string;
    ubicacion: string;
    foto: string;
}
