export class Rutina{
    constructor(_nombre: string,
               _desc:string, 
               _repeticiones:number,
               _duracion:number,
               _descanso: number){
            this.nombre = _nombre;
            this.desc = _desc;
            this.repeticiones = _repeticiones;
            this.duracion = _duracion;
            this.descanso = _descanso;
    
    }
    nombre: string;
    desc: string;
    repeticiones: number;
    duracion: number;
    descanso: number;
}
